**v2.0**

New icons: ffreddit, overchan, floating stickies, imsicatcher, quickdroid, logicaldefense, fmradio

Fix icon: tutanota

Delete bootanimation (for not to do the theme so overweight)

New ringtone

General icons revamping 

Shipping svgs

Tweak TwelF/Onze icon

**v1.4**

hotfix

**v1.3**

New icons: portauthority, vespucci, keypadmapper, wheelmap, miniopenwlanmap, openwlanmap, mozillastumbler, radiobeacon, yubico, wallabag, jamendo, wikimedia, wiktionary, sgit, agit, dashclock, cidrcalc, repay, contactmerger, callerid, timeriffic, worktime, openredmine, puma, disablemanager, applicationsinfo, gpslogger, orfox, beamshare, providersmedia, cmaccount, bugreport, oandbackup, slightbackup, batteryfu, cachacleaner, wifimatic, wally, soundrecorder, aizoban, seafile, skymap, minumanga, wally, atarashii, exchange

Tweak icons: smssecure, conversations

**v1.2**

Remake: webopac, mybanq, gadgetbridge

New icons: mercury, clover, movim, materialplayer, lcamera, gigaget, uget, maxima, tickmate, vanillamusic, exposed, yahtzee, callrecorder, acal, acaldav, caldavsync, mobilewebcam, networklog, gltron, tuxrider, autostarts, addi, aard, minilens

Some new system icons

Fix alarm and ringtone

**v1.1**

New icons: yahtzee, callrecorder, movim, acal, acaldav, caldavsync, mobilewebcam, networklog, gltron, tuxrider, autostarts, addi, aard, localgsm, unifiednlplegacy, applenlp, localwifinlp, mozillanlp, nominatimnlp, openbmapnlp, gsmlocation, quicklyric, brouter, atomic, i2p, turboeditor, tutanota, redphone, synergy, tasks, sfen, mitzuli, wifiwarning, writeily, fblite, some menu icons, superuser, effalerts, notepad, ionotepad, octodroid, tuner, yaaic, mybanq, schessclock, chesswatch, mibandnotifier, syncthing

**v1.0**

New icons: rmaps, tabulae, libreoffice, ametro, simpletask, solitairecg, solitaire, bubble, moneybalance, networkdiscovery, remotekeyboard, trickytripper, mirakel, wifikeyboard, 2048game, androidvnc, calendarwidget, dspmanager, easydice, equate, gameboid, pedometer, pixeldungeon, torch, wakeonlan, osmtracker, shatteredpixel, standalonecalendar, osmuploader, osmbugs, firenightly, minetest, freeminer, kore, kontalk, liberario

Remake: navit, droidfish

Fix icons: bitconium, coinbase, greenaddress, greenbits, testnet3, ftpserver

Alarm & Ringtone

**v0.8**

New icons: getbackgps, apktrack, prefmanager, ankidroid, openimgur, anymemo, keepass2android, duckduckgo, gloomydungeons, wificamera, sendwithftp, myexpenses, gnucash, sudowars, mathdoku, sudoq, opesudoku, sudokufree, soundwaves, sipdroid, maxsmain, lildebi, botbrew, debiandroid, debiankit,authenticator, quasseldroid, authenticator, navit, lumicall, linphone, ministro, frozenbuble, fastergps, focal, blockinger

Remake: antennapod, podax, carcast, davdroid, webopac

Fix icons: lockclock, browser, keepassdroid, owncloudsms

**v0.7**

New icons: swiftnotes, quickdic, gappsbrowser, webopac, quranandroid, synapse, otaupdate, bookmarkbr

Remake icons: fdroid, owncloudnews, lockclock, trebuchet

**v0.6**

New icons: smssecure, textsecure, opentraining, budget, androidrun, droidfish, chess, chesswalk, lightningbrowser, tintbrowser, ftpserver, androisens, tinfoilfb, tinfoiltw, vxconnectbot, irssiconnectbot

Remake icons: lockclock, kerneladiutor, myownnotes, nlpconfig, owncloudnews, owncloudsms, redreader, systemappmover, terminal, terminalemulator

**v0.5**

Fix icons: simtk, clocklock, openvpn, terminalemulator, unifiednlp, github

**v0.4**

New icon for TwelF and Onze

New icons: andbible, ask, communitycompass, compass, dictionaryformids, documents, dsub, flym, gadgetbridge, ghostcommander, github, hackerskeyboard, identiconizer, jadcompass, jupiterbroadcast, justplayer, lockclock, netmbuddy, nicecompass, nlpconfig, openvpn , openvpnsettings, osmonitor, osmonitor, plumble, redreader, simplealarm, subsonic, syncthing, terminal, terminalemulator

Fix icons: keyboard, wikipedia

Remake icons: apps, csipsimple , kerneladiutor, myownnotes, orbot, orwall, orweb, owncloudnewsreader, owncloudsms, performancecontrol, pftpd, systemappmover, telegram, xbmc

**v0.3**

New icons: adaway, opencamera, chatsecure, documentviewer, opendocumentreader, droidwall, apg, openkeychain, kdeconnect, linconnect, alogcat, alogcatroot, catlog, mythdroid, mythmote, keeppassdroid, passandroid, apvpdfreader, mupdf, pdfreader, vudroid, pftpd, antennapod, carcast, podax, ppsspp, sismicsreader, feedhive, ttrss, sparserss, simplerss, newsblur, andstatus, diasporawebclient, friendica, impeller, mustard, mustardmod, systemappmover, themes, orbot, orwall, orweb, transdroid, transdrone, transdroidsearch, trebuchet, xdafeedreader, tigase, conversations, beem, bombusmod, xabber, yaxim, amaze, barcodescanner, smsbackup+, palemoon, icecat, fileexplorer, filemanagerpro, kerneladuitor


Wallpaper and Lockscreen

Fix icon: davdroid

**v0.2**

Fixed critical issue 2

Fixed icons: mail, k9mail, soundrecorder, vlc, afw+

New icons: bitcoin, bitconium, coinbase, greenaddress, greenbits, testnet3, bitmask, hn, myownnotes, owncloudnewsreader, freeotp, cool reader, epub3reader, fb reader, page turner, xbmc, wifi privacy police, satstat, sms, csipsimple

Bootanimation

**v0.1**

Icons: alarm, apps, audiofx, broadcast, browser, calculator, calendar, camera, clock, connectbot, contacts, davdroid, downloads, fdroid, fennec, filemanager, firefox, firewall, gallery, help, k9mail, keiboard, mail, music, osmand, owncloud, phone, settings, simtk, telegram, twelf, vlc, voicerecorder, wikipedia, wordpress
