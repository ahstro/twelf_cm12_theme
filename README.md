[<img src="https://gitlab.com/xphnx/twelf_cm12_theme/uploads/6048c19d11fa071bf67704eb50c9f120/wiki_icon.png" 
alt="wiki_icon" width="66" />](https://gitlab.com/xphnx/twelf_cm12_theme/wikis/home)  [<img src="https://gitlab.com/xphnx/twelf_cm12_theme/uploads/dd41251de5768e0f2e96460d5b6483f0/issues_icon.png" 
alt="issues_icon" width="66" />](https://gitlab.com/xphnx/twelf_cm12_theme/issues)  [<img src="https://gitlab.com/xphnx/twelf_cm12_theme/uploads/91a9e58e936fd4e89fadff451a75b068/faq_icon.png" 
alt="faq_icon" width="66" />](https://gitlab.com/xphnx/twelf_cm12_theme/wikis/faq)

# TwelF - CM12 FLOSS Theme

TwelF is a Material Design inspired theme for Android Lollipop aiming to provide a consistent and minimalistic look to your device.

## Requirements

* A device running the Cyanogenmod 12 ROM (See the [list of compatible devices](https://gitlab.com/xphnx/twelf_cm12_theme/wikis/compatible-devices))
* [F-droid client](https://f-droid.org/) installed (optional, but strongly recommended)

## Features

* FLOSS Icon Pack
* Bootanimation
* Wallpaper & Lockscreen 
* Alarm & Ringtone

##### [Changelog](https://gitlab.com/xphnx/twelf_cm12_theme/blob/master/CHANGELOG.md)


## Installation

[![Get_it_on_F-Droid.svg](https://gitlab.com/uploads/xphnx/twelf_cm12_theme/a4649863bd/Get_it_on_F-Droid.svg.png)](https://f-droid.org/app/org.twelf.cmtheme)

## More info

 * If you are looking for an icon template or some howtos, go to the [**WIKI**](https://gitlab.com/xphnx/twelf_cm12_theme/wikis/home)
 * For issues, comments or icon request, please use the [**ISSUE TRACKER**] (https://gitlab.com/xphnx/twelf_cm12_theme/issues)

## Snapshots

<img src="https://gitlab.com/xphnx/twelf_cm12_theme/uploads/97c6faf3cad4619e8079327a5e3d3ac4/Screenshot_2015-05-23-07-53-03.png" 
alt="with a dark background" width="300" />
<img src="https://gitlab.com/xphnx/twelf_cm12_theme/uploads/b0ef81d60e8f4470e41cfec54c4a85b0/Screenshot_2015-05-23-21-03-30.png" 
alt="into apex launcher" width="300" />

<img src="https://gitlab.com/xphnx/twelf_cm12_theme/uploads/081953c26fe1f8d30276f1d16bb0f672/Screenshot_2015-05-22-10-51-04.png" 
alt="light background" width="300" />
<img src="https://gitlab.com/xphnx/twelf_cm12_theme/uploads/cec2077cb5bb09008b98d7c8681af67c/Screenshot_2015-05-22-23-47-06.png" 
alt="apps settings" width="300" />

<img src="https://gitlab.com/xphnx/twelf_cm12_theme/uploads/27787db387074995a36f18c262f4abba/Screenshot_2015-06-09-22-21-20.png" 
alt="share feneec" width="300" />
<img src="https://gitlab.com/xphnx/twelf_cm12_theme/uploads/a49b1be4708a70c2e3c554342ba21edb/Screenshot_2015-05-22-23-55-18.png" 
alt="inside afw++" width="300" />

## License 

[GPLv3](http://www.gnu.org/licenses/gpl-3.0.html) and other libre or open licenses for the artwork (see [LICENSE](https://gitlab.com/xphnx/twelf_cm12_theme/blob/master/LICENSE.md) for more details).
