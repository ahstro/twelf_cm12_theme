The code of this app is licensed under GPLv3, based on the code of Cyanogenmod Theme Engine Template (Apache v2).

The artwork are licensed under Creative Commons Attribution-NonComercial-ShareAlike 4.0 International except for the material released under another not compatible license.

**Bootanimation**

William Theaker/Robert Martinez(CC BY-SA 3.0): F-droid Logo

**Alarm**

Soldiah Beez - Mineral K High Time (CC BY-NC –SA)

**Ringtone**

No time Ft Marvellous benji - Dub terminator remix (CC BY-NC –SA)

**Icon Licenses**

xphnx (BY-NC-SA 4.0): jadcompass, compass, communitycompass, nicecompass, gadgetbridge, opentraining, budget, androidrun, tinfoilfb, tinfoiltw, lockclock, quranandroid, synapse, prefmanager, gloomydungeons, sudowars, mathdoku, sudoq, opesudoku, sudokufree, maxsmain, frozenbuble, blockinger, ametro, solitairecg, solitaire, bubble, networkdiscovery, mirakel, easydice, pixeldungeon, shatteredpixel, minetest, freeminer, 2048game, atomic, i2p, synergy, writeily, fblite, notepad, ionotepad, tuner, mibandnotifier, tickmate, torch, imsicatcher, ffreddit, mercury

Google (CC BY-SA 4.0 International): apps, alarm, browser, calendar, camera1, camera2, cellbroadcast, clock, connectbot, contacts, davdroid, downloads, fennec, firefox, filemanager, firewall, gallery, help, mail, lkeyboard, osmand, owncloud, phone, settings, sms, soundrecorder, xbmc, myownotes, owncloudnewsreader, otp, ebook, cipsimple, wppolice, satstat, adaway, opencamera, chatsecure, droidwall, kdeconnect, deskcon, linconnect, mythdroid, mythmote, keepassdroid, passandroid, pftpd, ppsspp, andstatus, mustard, impeller, diasporawebclient, friendica, twidere, mustardmod, systemappmover, orweb, tigase, conversations, beem, bombusmod, xabber, yaxim, amaze, fileexplorer, smsbackup, filemanagerpro, kerneladiutor, palemoon, icecat, andbible, ask, csipsimple, dictionaryformids, dsub, flym, ghostcommander, 
hackerskeyboard, justplayer, kerneladiutor, lkeyboard, myownnotes, netmbuddy, nlpconfig, orweb, osmonitor, owncloudnewsreader, owncloudsms, performancecontrol, pftpd, simplealarm, systemappmover, documents, xbmc, smssecure, textsecure, lightningbrowser, tintbrowser, ftpserver, androsens, vxconnectbot, irssiconnect, webopac, quickdic, gappsbrowser, otaupdate, bookmarkff, bookmarkbr, apktrack, getbackgps, wificamera, antennapod, podax, carcast, soundwaves, sendwithftp, gnucash, authenticator, navit, lumicall, linphone, fastergps, focal, rmaps, tabulae, simpletask, remotekeyboard, trickytripper, wifikeyboard, standalonecalendar, osmtracker, osmuploader, osmbugs, calendarwidget, osmtracker, firenightly, brouter, turboeditor, redphone, tasks, mitzuli, wifiwarning, yaaic, mybanq, schessclock, chesswatch, lcamera, xposed, vanillamusic, wallpaper, bluetooth, tags, cmupdater, callrecorder, acal, acaldav, caldavsync, networklog, autostarts, aard, jamendo, beamshare, cidrcalc, contactmerger, callerid, puma, applicationsinfo, disablemanager, oandbackup, slightbackup, batteryfu, minumanga, soundrecorder, wally, providersmedia, exchange, floatingstickies, overchan, materialplayer, movim

Cyanogenmod (Apache v2): audiofx, themes, onze, twelf, trebuchet

Cyanogenmod (GPL): whisperpush

Austin Andrews @templarian (SIL Open Font License 1.1): calculator, wikipedia, bitcoin, bitconium, coinbase, greenaddress, greenbits, testnet3, documentviewer, opendocumentreader, apg, openkeychain, apvpdfreader, mupdf, pdfreader, vudroid, orbot, orwall, barcodescanner, systemappmover, apktrack, wificamera, wifikeyboard, maxima, addi, cidrcalc, repay, timeriffic, worktime, cachecleaner, wifimatic

William Theaker/Robert Martinez(CC BY-SA 3.0): fdroid

Gamma-aspirin (GFDL): music

Telegram LLC (GPLv3): telegram

Wordpress (GPL): wordpress

Wikipedia (CC BY-SA 3.0): wikipedia, wiktionary

Wikipedia (Trademark policy): wikimedia

Angelus (CC BY-SA 3.0): bitmask

Ocal (Public Domain): adaway, chess, chesswalk, droidfish

Gabriel @lastrosestudios (SIL Open Font License 1.1): smsbackup, sismicsreader, feedhive, ttrss, sparserss, ttrss, simplerss, newsblur,

Chris Litherland (SIL Open Font License 1.1): mobilewebcam

Yasmina Lembachar (SIL Open Font License 1.1): yahtzee

Sun Microsystems (Apache v2): opendocumentreader

Mysitemyway (Public Domain): alogcat, alogcatroot, catlog

GNU (GPLv3) andstatus, mustard, mustardmod

Friendica (AGPL): friendica

Twidere/lordfriend (GPLv3): twidere

Delapouite (CC BY 3.0): trebuchet

Xmpp Standards (MIT): tigase, conversations, beem, bombusmod, xabber, yaxim

Syncthing (MPLv2): syncthing

Larry Ewing, Simon Budig, Anja Gerwinski (Attribution) : kerneladiutor

OpenVPN Technologies (CC BY-SA 3.0): openvpn, openvpnsettings

Mumble (BSD): plumble

Rama (CC BY-SA 2.0 fr): subsonic

Identiconizer (Apache v2): identiconizer

VLC (GPLv3): vlc

Swiftnotes (Apache v2): swiftnotes

Keepass2android (GPLv2): keepass2android

Anki (GPLv3): ankidroid

Anymemo (GPLv2): anymemo

Openimgur (Apache v2): openimgur

jmtrivial.info (GPL): sipdroid

Duckduckgo (Apache v2): duckduckgo

Software in the Public Interest, Inc (CC BY-SA 3.0): lildebi, botbrew, debiandroid, debiankit

Quasseldroid (GPLv3): quasseldroid

KDE community (GPLv3): ministro

WebUpd8 (CC BY-NC-SA): libreoffice

Watabou (GPLv3): pixeldungeon, shatteredpixel

unholybanquet (not provided): dspmanager, equate, gameboid, pedometer, wakeonlan

Liberario (GPLv3): liberario

Kontalk (): kontalk

Tutanota (GPLv3): tutanota

Sfen (): sfen

Font awesome (Public Domain): github, octodroid

Gigaget (GPLv3+): gigaget

Uget (Lesser GPL): uget

OpenStreetMap (CC BY-SA): gltron

Cheese (Public Domain): tuxrider

Jason Long (CC BY 3.0 Unported): sgit

Mathiasgebbe (CC BY 3.0 Unported): puma

Martin Herr (CC-BY-SA 2.5): openredmine

Vlad Marin (Attribution): logicaldefense

Simpleicon.com (Attribution): fmradio

Freepick (Attribution): overchan

Celli (Public Domain): overchan

Nevit Dilmen (CC BY-SA): clover

